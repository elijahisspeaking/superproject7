// SuperProject7.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

class Animal
{
    private:

    public:
       virtual void Voice() const = 0;
       virtual ~Animal() { std::cout << "class Animal Destructor" << "\n"; };
};

class Dog : public Animal
{
    public:
        virtual void Voice() const { std::cout << "The dog says: Bark!" << "\n"; }
        virtual ~Dog() { std::cout << "Destructor1" << "\n"; };
};

class Cat : public Animal
{
    public:
        virtual void Voice() const { std::cout << "The cat says: Meow!" << "\n"; }
        virtual ~Cat() { std::cout << "Destructor2" << "\n"; };
};

class Cow : public Animal
{
    public:
        virtual void Voice() const { std::cout << "The cow says: Muuuu!" << "\n"; }
        virtual ~Cow() { std::cout << "Destructor3" << "\n"; };

};
int main()
{
    Dog dog;
    Cat cat;
    Cow cow;

    const int N = 3;
    Animal* array[3];   
    array[0] = &dog;
    array[1] = &cat;
    array[2] = &cow;

    for (int i = 0; i < N; i++)
    {
        array[i]->Voice();
    }
    return EXIT_SUCCESS;
} 

 